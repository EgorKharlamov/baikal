var gulpversion = '4';
var gulp         = require("gulp"),
    gutil        = require("gulp-util"),
    changed      = require("gulp-changed"),
    pug          = require("gulp-pug"),
    sass         = require("gulp-sass"),
    autoprefixer = require("gulp-autoprefixer"),
    nano         = require('gulp-cssnano'),
    shorthand    = require('gulp-shorthand'),
    uncss        = require('gulp-uncss'),
    cleancss     = require("gulp-clean-css"),
    browserSync  = require("browser-sync"),
    fileSize     = require("gulp-filesize"),
    imagemin     = require("gulp-imagemin"),
    rename       = require("gulp-rename"),
    notify       = require("gulp-notify"),
    uglify       = require("gulp-uglify"),
    concat       = require('gulp-concat');
    //clean        = require("gulp-clean");

gulp.task("browser-sync", function() {
  browserSync({
    server: {
      baseDir: 'app'
    },
    notify: false,
  })
});

/* gulp.task("clean", function() {
  return gulp.src("app/")
  .pipe(clean())
}); */

gulp.task("css", function() {
  return gulp.src([
    'node_modules/bootstrap/scss/bootstrap-grid.scss',
    'node_modules/bootstrap/scss/bootstrap.scss',
    "src/sass/**/*.sass"])
  .pipe(changed('app/css', {extension: '.css'}))
  .pipe(sass().on("error", sass.logError))
  .pipe(concat('main.css'))
  .pipe(autoprefixer(["last 20 versions"]))
  .pipe(gulp.dest('app/css'))
  .pipe(fileSize())
  .pipe(uncss({
    html: [
      'http://localhost:3000/',
      'http://localhost:3000/about.html',
      'http://localhost:3000/contacts.html'
    ],
    ignore: [
      /\.affix/,
      /\.alert/,
      /\.close/,
      /\.collaps/,
      /\.fade/,
      /\.has/,
      /\.help/,
      /\.in/,
      /\.modal/,
      /\.open/,
      /\.popover/,
      /\.tooltip/
    ]
  }))
  .pipe(shorthand())
  .pipe(cleancss( {level: { 2: { removeDuplicateRules: true} } } ))
  .pipe(nano())
  .pipe(rename( {suffix: ".min", prefix: ""} ))
  .pipe(gulp.dest('app/css'))
  .pipe(fileSize())
  .pipe(browserSync.stream(true))
});

gulp.task("html", function() {
  return gulp.src("src/**/*.pug")
  .pipe(changed('app', {extension: '.html'}))
  .pipe(pug())
  .pipe(gulp.dest("app/"))
  .pipe(browserSync.reload({ stream: true }))
});

gulp.task("img", function() {
  return gulp.src("src/img/*")
  .pipe(changed('app/img'))
  .pipe(imagemin([
    imagemin.gifsicle({interlaced: true}),
    imagemin.jpegtran({progressive: true}),
    imagemin.optipng({optimizationLevel: 5}),
    imagemin.svgo({
        plugins: [
            {removeViewBox: true},
            {cleanupIDs: false}
        ]
    })
  ]))
  .pipe(gulp.dest("app/img"))
  .pipe(browserSync.reload({ stream: true }))
});

gulp.task("fonts", function() {
  return gulp.src("src/fonts/**/*.*")
  .pipe(changed('app/fonts'))
  .pipe(gulp.dest("app/fonts/"))
  .pipe(browserSync.reload({ stream: true }))
});

gulp.task("js", function() {
  return gulp.src("src/js/**/*.js")
  .pipe(changed('app/js'))
  //.pipe(uglify())
  .pipe(gulp.dest("app/js"))
  .pipe(browserSync.reload({ stream: true }))
});

gulp.task("watch", function() {
  gulp.watch("src/sass/**/*.sass", gulp.parallel("css"))
  gulp.watch("src/img/**/*.*", gulp.parallel("img"))
  gulp.watch("src/fonts/**/*.*", gulp.parallel("fonts"))
  gulp.watch("src/*.pug", gulp.parallel("html"))
  gulp.watch("src/js/*.js", gulp.parallel("js"))
});

gulp.task("default", gulp.parallel("css", "html", "img", "fonts", "js", "watch", "browser-sync"));